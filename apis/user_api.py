import json
from flask import request
from datetime import datetime, timedelta
from flask_restful import Resource, reqparse, abort

from app import api, db

from models import User, Request

parser = reqparse.RequestParser()

parser.add_argument('firstname')
parser.add_argument('lastname')
parser.add_argument('email')

#NOTE It isn't required to use sqlalchemy for this.
# could just as easily use sql in the "get, post, patch" methods  


class UserAdd(Resource):
    def post(self):
        '''add user'''
        args = parser.parse_args()
        user = User(
            firstname=args['firstname'],
            lastname=args['lastname'],
            email=args['email'])
        db.session.add(user)
        db.session.commit()

        return user.to_dict();


class UserMod(Resource):
    def get(self, user_id):
        '''get info'''
        return User(id=user_id) 

    def put(self, user_id):
        '''update user'''
        args = parser.parse_args()
        user = db.session.query(User).filter_by(id=user_id).first()
        user.firstname = args['firstname'] 
        user.lastname = args['lastname'] 
        user.email = args['email'] 
        db.session.commit()

    def delete(self, user_id):
        '''delete user'''
        user = db.session.query(User).filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()


class AllUsers(Resource):
    '''show info for users. accept get and post with no mods'''
    def show_all(self):
        users = User.query.order_by(User.firstname).all() 
        return [i.to_dict() for i in users] 
    
    def get(self):
        return self.show_all()

    def post(self):
        return self.show_all()
    

#url routes
api.add_resource(UserAdd, '/add_user') 
api.add_resource(AllUsers, '/users') 
api.add_resource(UserMod, '/user/<string:user_id>') 
