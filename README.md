
# Simple crud app with flask restful api and sample website interaction


## Install needed packages (in virtualenvironment is better of course).

    * `pip install -r requirements.txt`     


## Init/Updgrade Database (purposefully left migration files out of intial repo, you can create).

   * `python manage.py db init`    
   * `python manage.py db migrate`    
   * `python manage.py db upgrade`        


## Run the webserver from manage.py. This will serve on Port 5000.

   * `python manage.py runserver`        


## In a different terminal/screen move into sample_website directory and start Python simple server (can't just hit index.html as file, it won't work).

    * `python -m SimpleHTTPServer 8000`    
    * Visit localhost:8000 in browser.   
