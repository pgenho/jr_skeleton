$(document).ready(function() {
    get_users();
});

function get_users() {
    $.get('http://localhost:5000/users', place_users);
}

function place_users(users) {
    //this will get executed after user data is retrieved.
    //show user rows as input
    $('#users').empty();

    for (var i = 0, l = users.length; i < l; i ++) {
        var u = users[i];

        var row = $('<div class="user-row"></div>');

        var first = $('<input>');
        var last = $('<input>');
        var email = $('<input>');
        var update = $('<button>Update</button>');
        var del = $('<button>Delete</button>');

        $(first).attr('value', u.firstname);
        $(last).attr('value', u.lastname);
        $(email).attr('value', u.email);

        $(row).append(first, last, email, update, del);
        
        //attach row to dom in "users" id'd element
        $('#users').append(row);

        //bind buttons to action
        $(update).bind('click', function() {
            update_user(this);
        });

        $(del).bind('click', function() {
            delete_user(this);
        });

        //attach user id to row data, don't show that
        $(row).data('userid', u.id);
    }
}


function submit_new_user() {
    //send new user to database
    var data = {
        firstname: $('#new-user-first').val(),
        lastname: $('#new-user-last').val(),
        email: $('#new-user-email').val()
    }
    var fields = $('#new-user').find('input');
    fields.each(function() {
        //blank inputs for next time
        $(this).val('');
    });

    //send the data to api endpoint then update user display
    $.post('http://localhost:5000/add_user', data, get_users); 
}

function update_user(button) {
    //get row button belongs to
    var row = $(button).closest('.user-row');
    var userid = $(row).data().userid;
    var fields = $(row).find('input');
    var data = {
        firstname: $(fields[0]).val(),
        lastname: $(fields[1]).val(),
        email: $(fields[2]).val()
    }
    $.ajax({
        type: "PUT",
        data: data,
        url: 'http://localhost:5000/user/' + userid,
        success: get_users
    });
}

function delete_user(button) {
    //get row button belongs to
    var row = $(button).closest('.user-row');
    var userid = $(row).data().userid;
    $.ajax({
        type: "DELETE",
        url: 'http://localhost:5000/user/' + userid,
        success: get_users
    });
}
