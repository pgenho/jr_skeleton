from app import db
import datetime


class User(db.Model):
    __tablename__ = 'user'
    def to_dict(self):
        return {i.name: getattr(self, i.name) for i in self.__table__.columns}

    #DB columns
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.Unicode, nullable=False)
    lastname = db.Column(db.Unicode, nullable=False)
    email = db.Column(db.String, nullable=True)


class Request(db.Model):
    __tablename__ = 'request'
    def to_dict(self):
        return {i.name: getattr(self, i.name) for i in self.__table__.columns}

    #DB columns
    id = db.Column(db.Integer, primary_key=True)
    requester = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    updated_by = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    description = db.Column(db.String(720))
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, onupdate=datetime.datetime.now)
