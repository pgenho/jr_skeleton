import flask
import flask_restful
from flask_cors import CORS
#from database import db
import flask_sqlalchemy
from configs import Config 

app = flask.Flask(__name__)
app.config.from_object(Config)

db = flask_sqlalchemy.SQLAlchemy(app)
db.init_app(app)

CORS(app)

api = flask_restful.Api(app)

#from apis.user_api import UserAdd, UserMods
#api.add_resource(UserAdd, '/user') 
#api.add_resource(UserMods, '/users/<string:user_id>') 
